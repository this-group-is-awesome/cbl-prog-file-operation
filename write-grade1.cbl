       IDENTIFICATION DIVISION.
       PROGRAM-ID. write-grade1.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           05 STU-ID   PIC X(8).
           05 MIDTERM-SCORE  PIC 9(2)V9(2).
           05 FINAL-SCORE    PIC 9(2)V9(2).
           05 PROJECT-SCORE  PIC 9(2)V9(2).
       PROCEDURE DIVISION .
       begin.
           OPEN OUTPUT SCORE-FILE 
           MOVE "39030001" TO STU-ID
           MOVE "34.05" TO MIDTERM-SCORE 
           MOVE "25.25" TO FINAL-SCORE
           MOVE "10.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160002" TO STU-ID
           MOVE "26.05" TO MIDTERM-SCORE 
           MOVE "29.25" TO FINAL-SCORE
           MOVE "15.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160851" TO STU-ID
           MOVE "31.56" TO MIDTERM-SCORE 
           MOVE "41.25" TO FINAL-SCORE
           MOVE "17.9" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160234" TO STU-ID
           MOVE "21.36" TO MIDTERM-SCORE 
           MOVE "23.95" TO FINAL-SCORE
           MOVE "12.1" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL 

           MOVE "62160643" TO STU-ID
           MOVE "15.56" TO MIDTERM-SCORE 
           MOVE "33.25" TO FINAL-SCORE
           MOVE "21.9" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL                       

           CLOSE SCORE-FILE 
           GOBACK 
           .   
